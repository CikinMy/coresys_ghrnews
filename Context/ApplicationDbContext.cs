﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TnbCorsys_Template.Entities;

namespace TnbCorsys_Template.Context
{
   
    public class ApplicationDbContext : IdentityDbContext<User, Role, string, IdentityUserClaim<string>,
                                IdentityUserRole<string>, IdentityUserLogin<string>,
                                IdentityRoleClaim<string>, IdentityUserToken<string>>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }
        public DbSet<User> AspNetUsers { get; set; }
        public DbSet<Role> AspNetRoles { get; set; }
       
    }
}
