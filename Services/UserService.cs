﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Data.SqlClient;
using TnbCorsys_Template.Entities;
using TnbCorsys_Template.Context;
using TnbCorsys_Template.Helpers;

namespace TnbCorsys_Template.Services
{
    public interface IUserService
    {
      
        IEnumerable<User> GetAll();      
        User GetByUsername(string username);      
        User Create(User user, string password);
        void Update(User user, string password = null);
        void Delete(int id);      
     
    }

    public class UserService : IUserService
    {
        private ApplicationDbContext _context;

        public UserService(ApplicationDbContext context)
        {
            _context = context;
        }
       
        public IEnumerable<User> GetAll()
        {
            return _context.AspNetUsers;
        }

        public User GetByUsername(string username)
        {
            return _context.AspNetUsers.Where(x => x.UserName == username).FirstOrDefault();
        }

        public User Create(User user, string password)
        {
            // validation
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");

            if (_context.AspNetUsers.Any(x => x.UserName == user.UserName))
                throw new AppException("Username \"" + user.UserName + "\" is already taken");

            //byte[] passwordHash, passwordSalt;
            //CreatePasswordHash(password, out passwordHash, out passwordSalt);

            //user.PasswordHash = passwordHash;
            //user.PasswordSalt = passwordSalt;

            _context.AspNetUsers.Add(user);
            _context.SaveChanges();

            return user;
        }

        public void Update(User userParam, string password = null)
        {
            var user = _context.AspNetUsers.Find(userParam.Id);

            if (user == null)
                throw new AppException("User not found");

            if (userParam.UserName != user.UserName)
            {
                // username has changed so check if the new username is already taken
                if (_context.AspNetUsers.Any(x => x.UserName == userParam.UserName))
                    throw new AppException("Username " + userParam.UserName + " is already taken");
            }

            // update user properties
            user.FullName = userParam.FullName;
            user.UserName = userParam.UserName;

            // update password if it was entered
            if (!string.IsNullOrWhiteSpace(password))
            {
                //byte[] passwordHash, passwordSalt;
                //CreatePasswordHash(password, out passwordHash, out passwordSalt);

                //user.PasswordHash = passwordHash;
                //user.PasswordSalt = passwordSalt;
            }

            _context.AspNetUsers.Update(user);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = _context.AspNetUsers.Find(id);
            if (user != null)
            {
                _context.AspNetUsers.Remove(user);
                _context.SaveChanges();
            }
        }


      

    }
}
