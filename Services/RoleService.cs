﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TnbCorsys_Template.Context;
using TnbCorsys_Template.Entities;
using TnbCorsys_Template.Helpers;

namespace TnbCorsys_Template.Services
{
    public interface IRoleService
    {
        IEnumerable<Role> GetAll();
        Role GetById(string id);
        Role GetByName(string name);
        Role Create(Role data);
        void Update(Role data);
        void Delete(string id);
    }
    public class RoleService : IRoleService
    {


        private ApplicationDbContext _context;
        public RoleService(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Role> GetAll()
        {
            return _context.AspNetRoles;
        }

        public Role GetById(string id)
        {
            return _context.AspNetRoles.Find(id);
        }
        public Role GetByName(string name)
        {
            return _context.AspNetRoles.Where(x => x.NormalizedName == name).FirstOrDefault();
        }
        public Role Create(Role data)
        {
            _context.AspNetRoles.Add(data);
            _context.SaveChanges();

            return data;
        }

        public void Update(Role data)
        {
            try
            {

                var details = _context.AspNetRoles.Find(data.Id);

                if (details == null)
                    throw new AppException("Role not found");

                // update Role properties
                details.Name = data.Name;
                details.NormalizedName = data.Name;

                _context.AspNetRoles.Update(details);
                _context.SaveChanges();
            }
            catch (AmbiguousMatchException MyException)
            {
                Console.WriteLine(MyException); // use it here
            }
        }

        public void Delete(string id)
        {
            var details = _context.AspNetRoles.Find(id);
            if (details != null)
            {
                _context.AspNetRoles.Remove(details);
                _context.SaveChanges();
            }
        }
    }
}
