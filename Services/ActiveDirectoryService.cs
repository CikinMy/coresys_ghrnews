﻿using Microsoft.AspNetCore.Identity;
using System;
using System.DirectoryServices;
using System.Threading.Tasks;
using TnbCorsys_Template.Entities;

namespace TnbCorsys_Template.Services
{
    public interface IActiveDirectoryService
    {
        Task<ActiveDirectoryUser> GetUserByStaffNo(string staffNo);
        Task<ActiveDirectoryUser> Authenticate(string staffNo, string password);
        Task<SignInResult> SignInAsync(string username, string password);
    }
    public class ActiveDirectoryService : IActiveDirectoryService
    {
        private readonly string _path;
        private readonly string _userDomain;
        private readonly string _adminUsername;
        private readonly string _adminPassword;

        public ActiveDirectoryService(string path, string userDomain, string adminUsername, string adminPassword)
        {
            _path = path;
            _userDomain = userDomain;
            _adminUsername = adminUsername;
            _adminPassword = adminPassword;
        }

        private DirectoryEntry GetDirectoryEntry(string username, string password)
        {
            try
            {
                DirectoryEntry de = new DirectoryEntry();
                de.Path = _path;
                de.AuthenticationType = AuthenticationTypes.Secure;
                de.Username = _userDomain + @"\" + username;
                de.Password = password;

                return de;
            }
            catch (Exception)
            {
                DirectoryEntry de = new DirectoryEntry();
                return de;
            }
        }

        private Task<SearchResultCollection> SearchAccount(string authUsername, string authPassword, string searchUsername)
        {
            DirectoryEntry de = GetDirectoryEntry(authUsername, authPassword);

            DirectorySearcher deSearch = new DirectorySearcher();
            deSearch.SearchRoot = de;
            deSearch.Filter = "(SAMAccountName=" + searchUsername + ")";

            return Task.FromResult(deSearch.FindAll());
        }

        public async Task<ActiveDirectoryUser> Authenticate(string username, string password)
        {
            try
            {
                var results = await SearchAccount(username, password, username);

                if (results.Count > 0)
                {
                    return BuildDirectoryUser(results[0]);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<ActiveDirectoryUser> GetUserByStaffNo(string staffNo)
        {
            var results = await SearchAccount(_adminUsername, _adminPassword, staffNo);

            if (results.Count > 0)
            {
                return BuildDirectoryUser(results[0]);
            }
            else
            {
                return null;
            }
        }

        //-------------------------------------------------------------------------------- 

        private ActiveDirectoryUser BuildDirectoryUser(SearchResult result)
        {
            // NOTE:
            //  -SAMAccountName untuk staff id
            //  - Property name->description      
            //  1. displayName/name ->full name
            //  2. mail->email
            //  3. mobile->telephone number
            //  4. title->designation(optional not mandatory)
            //  5. thumbnailPhoto->gambar(type OctetString)

            var u = new ActiveDirectoryUser
            {
                StaffNo = ReadStringProperty(result, "SAMAccountName"),
                FullName = ReadStringProperty(result, "displayName"),
                Email = ReadStringProperty(result, "mail"),
                PhoneNumber = ReadStringProperty(result, "mobile")
            };

            var bytes = ReadBytesProperty(result, "thumbnailPhoto");
            if (bytes != null && bytes.Length > 0)
            {
                u.Photo = "data:image/jpg;base64," + Convert.ToBase64String(bytes);
            }
            else
            {
                u.Photo = "";
            }

            return u;
        }

        public string ReadStringProperty(SearchResult result, string propertyName)
        {
            try
            {
                return (string)result.Properties[propertyName][0];
            }
            catch
            {
                return "";
            }
        }

        public byte[] ReadBytesProperty(SearchResult result, string propertyName)
        {
            try
            {
                return (byte[])result.Properties[propertyName][0];
            }
            catch
            {
                return new byte[] { };
            }
        }
        public async Task<SignInResult> SignInAsync(string username, string password)
        {
            var user = await Authenticate(username, password);
            if (user != null)
            {
                return SignInResult.Success;
            }
            else
                return SignInResult.Failed;
        }
    }
}
