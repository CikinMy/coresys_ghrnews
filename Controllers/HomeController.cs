﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using TnbCorsys_Template.Dtos;
using TnbCorsys_Template.Entities;
using TnbCorsys_Template.Models;
using TnbCorsys_Template.Services;

namespace TnbCorsys_Template.Controllers
{
    public class HomeController : Controller
    {
        private readonly SignInManager<User> _signInManager;     
        private readonly IAuthorizationService _authorization;
        private readonly IActiveDirectoryService _activeDirectoryService;
        private UserManager<User> _userManager;
        private ISecurityService _securityService;
        private IUserService _userService;     
        private IWebHostEnvironment _host;
        private IConfiguration _config;
        public HomeController(
           SignInManager<User> signInManager,
           UserManager<User> userManager,
           IAuthorizationService authorization,
            IActiveDirectoryService activeDirectoryService,          
            ISecurityService securityService,
            IUserService userService,
            IWebHostEnvironment host,
            IConfiguration config
           )
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _authorization = authorization;
            _activeDirectoryService = activeDirectoryService;          
            _userService = userService;
            _securityService = securityService;
          
            _host = host;
            _config = config;
        }


        public IActionResult Index()
        {          
                return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        [AllowAnonymous]
        [HttpPost("/Login")]
        public async Task<ActionResult> Login(UserDto userDto)
        {
            var adUser = await _activeDirectoryService.SignInAsync(userDto.Username, userDto.Password);       
        
            if (adUser.Succeeded)
            {
                
               return Ok(new
               {
                   response = StatusCode(StatusCodes.Status200OK),
                   url = ""
               });
            }
            else
            {
                return BadRequest(new { message = "Error!. Authentication AD fail" });
            }




        }
       
        [HttpGet("/LogOut")]
        public async Task<IActionResult> LogOut()
        {
            try
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                await _signInManager.SignOutAsync();
                //_logger.LogInformation("User logged out.");
            }
            catch (Exception) { }

            return LocalRedirect("/Home/Index");
        }
    }
}
