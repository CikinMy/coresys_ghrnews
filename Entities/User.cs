﻿using Microsoft.AspNetCore.Identity;
using System;


namespace TnbCorsys_Template.Entities
{
    public class User : IdentityUser
    {
        public override string Id { get; set; }
        //public string Username { get; set; }
        public string FullName { get; set; }       
        public new byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public int Religion { get; set; }
    }
}
