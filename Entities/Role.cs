﻿using Microsoft.AspNetCore.Identity;

namespace TnbCorsys_Template.Entities
{
    public class Role : IdentityRole
    {
        public override string Id { get; set; }
        public override string Name { get; set; }
        public override string NormalizedName { get => base.NormalizedName; set => base.NormalizedName = value; }

    }
}
