﻿using Newtonsoft.Json;

namespace TnbCorsys_Template.Dtos
{
    public class RoleDto
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "normalizedName")]
        public string NormalizedName { get; set; }
    }
}
