﻿using System;

namespace TnbCorsys_Template.Dtos
{
    public class UserDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public string Param { get; set; }
        public int Religion { get; set; }
    }
}
