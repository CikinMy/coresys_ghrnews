#pragma checksum "D:\All PROJECTs\Workspace-Emp Enterprise App\AD_TEMPLATE\TnbCorsys_Template\Views\Shared\_navbar.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "777f170685839c417a982fc92b46104bdf8b0add"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__navbar), @"mvc.1.0.view", @"/Views/Shared/_navbar.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\All PROJECTs\Workspace-Emp Enterprise App\AD_TEMPLATE\TnbCorsys_Template\Views\_ViewImports.cshtml"
using TnbCorsys_Template;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\All PROJECTs\Workspace-Emp Enterprise App\AD_TEMPLATE\TnbCorsys_Template\Views\_ViewImports.cshtml"
using TnbCorsys_Template.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"777f170685839c417a982fc92b46104bdf8b0add", @"/Views/Shared/_navbar.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"978806f6125f40cd98d23d7ffa882e18847d4a62", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__navbar : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<nav class=""navbar navbar-header navbar-expand-lg"" data-background-color=""white"">

    <div class=""container-fluid"">
        
        <ul class=""navbar-nav topbar-nav ml-md-auto align-items-center"">

            <li class=""nav-item dropdown hidden-caret"">
                <a class=""nav-link dropdown-toggle"" href=""#"" id=""notifDropdown"" role=""button"" data-toggle=""dropdown"" aria-haspopup=""true"" aria-expanded=""false"">
                    <i class=""fa fa-bell""></i>
                    <span class=""notification"">4</span>
                </a>
                <ul class=""dropdown-menu notif-box animated fadeIn"" aria-labelledby=""notifDropdown"">
                    <li>
                        <div class=""dropdown-title"">You have 4 new notification</div>
                    </li>
                    <li>
                        <div class=""notif-scroll scrollbar-outer"">
                            <div class=""notif-center"">
                                <a href=""#"">
                                  ");
            WriteLiteral(@"  <div class=""notif-icon notif-primary""> <i class=""fa fa-user-plus""></i> </div>
                                    <div class=""notif-content"">
                                        <span class=""block"">
                                            New user registered
                                        </span>
                                        <span class=""time"">5 minutes ago</span>
                                    </div>
                                </a>
                                <a href=""#"">
                                    <div class=""notif-icon notif-success""> <i class=""fa fa-comment""></i> </div>
                                    <div class=""notif-content"">
                                        <span class=""block"">
                                            Rahmad commented on Admin
                                        </span>
                                        <span class=""time"">12 minutes ago</span>
                                    </div>
       ");
            WriteLiteral(@"                         </a>
                                <a href=""#"">
                                    <div class=""notif-img"">
                                        <img src=""../assets/img/profile2.jpg"" alt=""Img Profile"">
                                    </div>
                                    <div class=""notif-content"">
                                        <span class=""block"">
                                            Reza send messages to you
                                        </span>
                                        <span class=""time"">12 minutes ago</span>
                                    </div>
                                </a>
                                <a href=""#"">
                                    <div class=""notif-icon notif-danger""> <i class=""fa fa-heart""></i> </div>
                                    <div class=""notif-content"">
                                        <span class=""block"">
                                            Farrah");
            WriteLiteral(@" liked Admin
                                        </span>
                                        <span class=""time"">17 minutes ago</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a class=""see-all"" href=""javascript:void(0);"">See all notifications<i class=""fa fa-angle-right""></i> </a>
                    </li>
                </ul>
            </li>

            <li class=""nav-item dropdown hidden-caret"">
                <a class=""dropdown-toggle profile-pic"" data-toggle=""dropdown"" href=""#"" aria-expanded=""false"">
                    <div class=""avatar-sm"">
                        <img src=""../assets/img/profile.jpg"" alt=""..."" class=""avatar-img rounded-circle"">
                    </div>
                </a>
                <ul class=""dropdown-menu dropdown-user animated fadeIn"">                   
           ");
            WriteLiteral(@"         <li>
                        <div class=""dropdown-divider""></div>
                        <a class=""dropdown-item"" href=""#"">My Profile</a>
                        <a class=""dropdown-item"" href=""#"">My Balance</a>
                        <a class=""dropdown-item"" href=""#"">Inbox</a>
                        <div class=""dropdown-divider""></div>
                        <a class=""dropdown-item"" href=""#"">Account Setting</a>
                        <div class=""dropdown-divider""></div>
                        <a class=""dropdown-item"" href=""#"">Logout</a>
                    </li>

                </ul>
            </li>

           
        </ul>
    </div>
</nav>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
